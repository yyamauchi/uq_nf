# Accelerating sampling on posterior distributions via normalizing flows

## Paper
If using code, or code derived from it, it may be appropriate to cite the following paper:
- [Normalizing Flows for Bayesian Poteriors: Reproducibility and Deployment](https://arxiv.org/abs/2310.04635)

## Requirements
The code in this repository requires a resonably up-to-date python3. The main packages used are `jax`, `equinox`, `optax`. The required packages are listed in detail in requirements.txt. To install libraries using `venv`, do:
```
python -m venv env
. env/bin/activate
pip install --upgrade pip
pip install jax
pip install equinox
pip install optax
```
and so on. 

## Workflow
The use of normalizing flows for sampling from posterior distributions consists of four steps:
1. Preparation: prepare the training data set
2. Training: train a normalizing flow with chosen hyperparameters 
3. Quantification: quantify the accuracy of the normalizing flow
4. Sampling: sample from the distribution using the normalizing flow
In the following, we detail each step.
 
## Preparation
To start, generate a data set to which you train a normalizing flow for. You can prepare own data set, or use `mcmc.py` to generate samples together with a model distribution in `models/` directory. In the training data file, each line should contain one sample, in which a configuration taken from your distribution is followed by the natural logarithm of the distribution value of the sample. For example, is a train data set for 3 dimensional Gaussian distribution, each line in the file should be of the form
```math
x_1,x_2,x_3,-(x_1^2 + x_2^2 + x_3^2)/2
```
To create a data file via `mcmc.py`, first create a model file, e.g.,
```
PG2d.Model(c = 1.0, c1 = 2.0, c2 = 3.0, c3 = 4.5, c4 = 3.0, c5 = 3.0, c6 = 5.0)
```
See models/params_pd2d for an example. 
Next,  run
```
./mcmc.py model
```
and samples will be printed out. Save the outputs using `-f` options. Pass in `-h` to other see options.

# Training
Given the train data set, train a normalizing flow for the training data with `nf.py` by
``` 
./nf.py traindata dim nf testdata
```
Here `dim` is the dimension of distribution, and `traindata` and `nf` are the filenames for training data set to read the samples from, and save the trained normalizing flow to. `testdata` is another set of samples one can pass in to compute the loss function with. When passing `-f 0`, the loss function will not be computed with `testdata`. When passing in `-f 10`, the loss function will be computed and printed out every 10 train steps with `testdata`.  Pass in `-h` to see options. One important feature is `--init`, by which one initialize the neural network to the one stored in the `nf` file, train, and output the resulting normalizing flow to the same file.

# Quantification
The normalizing flow you just trained only approximately yield the target distribution you specified in the `traindata` file. To visually evaluate the fit, use `visual.py` as
```
.visual.py nf traindata -n <number of samples from nf to compare> -vfn <saved image file name>
```
which saves a corner plot comparing a trained nf, `nf`, to a dataset with matching dimension, `traindata`.

# Sampling
Finally, collect samples from the model distribution by `sample.py`:
```
./sample.py nf
```
, which prints out the samples from the distribution induced by the normalizing flow in `nf`. Pass in `-h` to see options.

# Example
```
mkdir -p data/
./mcmc.py models/model_pg2d -n 1000 -c 10 --seed 0 > data/traindata.dat
./mcmc.py models/model_pg2d -n 1000 -c 10 --seed 1 > data/testdata.dat
./train.py data/traindata.dat 2 data/nf data/testdata.dat  -l 6 -s 1000 -f 100
./visual.py data/nf data/traindata.dat -n 1000 -fn corner_plot.png
./sample.py data/nf -S 10000 > data/samples.dat
```
In this example, the training data (test data) is created from the PG2d distribution and saved in `data/training.dat` (`data/testdata.dat`). A normalizing flow is trained with 6 layer of Real NVP neural network for 1000 training steps. At each training step, randomly taken 1000 samples from the training data are used to estimate the loss function. The test data is used every 100 steps to compute the loss function. The normalizing flow is then evaluated by saving various metrics and a corner plot. Lastly, 10000 samples are taken by the normalizing flow and stored in `data/samples.dat`. To see more options, pass in `-h` for each script.

## Organization

The main scripts are:

- **`mcmc.py`**: Creates MCMC samples from a given model
- **`nf.py`**: Trains a normalizing flow 
- **`sample.py`**: Takes samples from the normalizing flow
- **`metrics.py`**: Computes metrics for quantifying the accuracy of the normalizing flow
- **`visual.py`**: Produces a corner plot comparing a trained normalizing flow to training data

The directory structure is:

- **`mc/`**: Various MCMC codes
- **`models/`**:Model distributions
- **`nn/`**: Neural networks
