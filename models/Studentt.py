from dataclasses import dataclass
from typing import Tuple
import numpy as np
import jax.numpy as jnp
from scipy.stats import multivariate_t

@dataclass
class Model:
    df: float
    sig1: float
    sig2: float
    cx: float
    cy: float
    dim = 2

    def __post_init__(self):
        self.correl = self.sig1*self.sig2*0.8
        self.matrix = np.linalg.inv(np.array([[self.sig1**2, self.correl],[self.correl, self.sig2**2]]))
        self.center = np.array([self.cx, self.cy])


    # returns the log of the distribution
    def dist(self, x):
        y = x - self.center
        return -(self.dim + self.df)/2.* np.log(1+(y@self.matrix@y)/self.df)


    def observables(self, x):
        return jnp.array([x[0], x[1], x[0]**2 + x[1]**2])

