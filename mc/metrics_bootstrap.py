#!/usr/bin/env python

# Libraries
import numpy as np

def metrics(data, nb, nr):
    """
    Computes and stores various metrics during training. 
    This function is meant to evaluate a trained nf.

    Args:
        data (str): Path to the data file to compare nf to.
        nb (int): Number of blocks to make in data
        nr (int): Number of resamplings to in bootstrap
    """

    # Read data from file
    xs = []
    with open(data, 'r') as f:
        for l in f.readlines():
            r = [np.float64(x) for x in l.split(' ')]
            xs.append(r)
    xs = np.array(xs)
    nsamples = np.shape(xs)[0]
    D = np.shape(xs)[1] 

    print(f'shape of data is {np.shape(xs)}')

    B = int(nsamples/nb)  # Number of samples per block

    print(f'{nsamples} samples are passed in, {B} samples per block')

    # Blocking
    xb1 = np.zeros((nb,D))
    xb2 = np.zeros((nb,D))
    xb3 = np.zeros((nb,D))
    xb4 = np.zeros((nb,D))
    xbc = np.zeros((nb,D,D))
    for i in range(nb):
        for j in range(D):
            xb1[i,j] = np.mean(xs[i*B:i*B+B,j])
            xb2[i,j] = np.mean(xs[i*B:i*B+B,j]**2)
            xb3[i,j] = np.mean(xs[i*B:i*B+B,j]**3)
            xb4[i,j] = np.mean(xs[i*B:i*B+B,j]**4)
            for k in range(D):
                xbc[i,j,k] = np.mean(xs[i*B:i*B+B,j]*xs[i*B:i*B+B,k])
   
    # Compute mean
    for i in range(D):
        ms = []
        for j in range(nr):
            s = np.random.choice(range(nb), nb)
            ms.append(np.mean(xb1[:,i][s]))
        ms = np.array(ms)
        mean = np.mean(ms)
        err = np.std(ms)
        print(f'mean of parameter {i} is {mean:.5f} \u00B1 {err:.5f}')

    # Compute variance
    for i in range(D):
        vs = []
        for j in range(nr):
            s = np.random.choice(range(nb), nb)
            x1new = xb1[:,i][s]
            x2new = xb2[:,i][s]
            meanb = np.mean(x1new)
            vs.append(np.mean(x2new - 2*meanb*x1new + meanb**2))
        vs = np.array(vs)
        mean = np.mean(vs)
        err = np.std(vs)
        print(f'variance of parameter {i} is {mean:.5f} \u00B1 {err:.5f}')
    
    # Compute skewness
    for i in range(0):
        ss = []
        for j in range(nr):
            s = np.random.choice(range(nb), nb)
            x1new = xb1[:,i][s]
            x2new = xb2[:,i][s]
            x3new = xb3[:,i][s]
            meanb = np.mean(x1new)
            varb = np.mean(x2new - 2*meanb*x1new + meanb**2)
            ss.append(np.mean((x3new - 3*meanb*x2new + 3*meanb**2*x1new - meanb**3)/varb**(3./2.)))
        ss = np.array(ss)
        mean = np.mean(ss)
        err = np.std(ss)
        print(f'skewness of parameter {i} is {mean:.5f} \u00B1 {err:.5f}')


    # Compute kurtosis (NOT Fisher)
    for i in range(0):
        ks = []
        for j in range(nr):
            s = np.random.choice(range(nb), nb)
            x1new = xb1[:,i][s]
            x2new = xb2[:,i][s]
            x3new = xb3[:,i][s]
            x4new = xb4[:,i][s]
            meanb = np.mean(x1new)
            varb = np.mean(x2new - 2*meanb*x1new + meanb**2)
            ks.append(np.mean((x4new - 4*meanb*x3new + 6*meanb**2*x2new - 4*meanb**3*x1new + meanb**4)/varb**2))
        ks = np.array(ks)
        mean = np.mean(ks)
        err = np.std(ks)
        print(f'kurtosis of parameter {i} is {mean:.5f} \u00B1 {err:.5f}')

    # Compute covariance
    for i in range(D):
        for k in range(i+1, D):
            cv = []
            for j in range(nr):
                s = np.random.choice(range(nb), nb)
                xinew = xb1[:,i][s]
                xknew = xb1[:,k][s]
                xiknew = xbc[:,i,k][s]
                cv.append(np.mean(xiknew - np.mean(xinew)*xknew - np.mean(xknew)*xinew + np.mean(xinew)*np.mean(xknew)))
            cv = np.array(cv)
            mean = np.mean(cv)
            err = np.std(cv)
            print(f'covariance of parameter {i} and {k} is {mean:.5f} \u00B1 {err:.5f}')

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
            description="Training normalizing flow",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            fromfile_prefix_chars='@'
            )
    parser.add_argument('data', type=str, help="model samples filename")
    parser.add_argument('-b', '--nblock', type=int, default=100, help="number of blocks in error analysis")
    parser.add_argument('-r', '--resample', type=int, default=500, help="Number of bootstrap resamplings")
    args = parser.parse_args()
    
    metrics(args.data, args.nblock, args.resample)








